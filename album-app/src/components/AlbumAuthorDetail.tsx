import "mobx-react-lite/batchingForReactDom";
import React from "react";
import Masonry from "react-masonry-css";
import { observer } from "mobx-react-lite";
import { useStore } from "../models";
import { Image } from "../models/Image";
import { Link, useParams } from "react-router-dom";
import AlbumNav from "./AlbumNav";
import "../css/Album.css";

export interface AuthorAlbumRouteParams {
  imageAuthor: string;
}

const AuthorAlbum = () => {
  const album = useStore();
  const { imageAuthor } = useParams<AuthorAlbumRouteParams>();

  return (
    <div>
      <AlbumNav />
      <div>
        <h2 className="author">Author : {imageAuthor}</h2>
        {album.imageList.map((imageItem: Image) =>
          imageItem.author === imageAuthor ? (
            <div>
              <img
                className="img_detail"
                src={`https://picsum.photos/id/${imageItem.id}/${imageItem.width}/${imageItem.height}`}
                alt={imageItem.id}
                width="777"
              />
            </div>
          ) : (
            <></>
          )
        )}
      </div>
    </div>
  );
};

export default observer(AuthorAlbum);
