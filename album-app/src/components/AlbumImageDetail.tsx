import React from "react";
import { Link, useParams } from "react-router-dom";
import { observer } from "mobx-react-lite";
import "../css/Album.css";
import { useStore } from "../models";
import { Image } from "../models/Image";
import AlbumNav from "./AlbumNav";

export interface ImageDetailRouteParams {
  id: string;
}

const ImageDetail = () => {
  const album = useStore();
  const { id } = useParams<ImageDetailRouteParams>();
  const image = album.imageList.find((item: Image) => id === item.id);

  if (image) {
    return (
      <div>
        <AlbumNav />
        <h2 className="author">Author : {image.author}</h2>
        <img
          className="img_detail"
          src={`https://picsum.photos/id/${image.id}/${image.width}/${image.height}`}
          alt={image.id}
          width="777"
        />
      </div>
    );
  }
  return (
    <div>
      <Link to="/">
        <h1 className="header"> React Photo Album </h1>
      </Link>
      <h2 className="author">No Image Found</h2>
    </div>
  );
};

export default observer(ImageDetail);
