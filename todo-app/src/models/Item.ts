import { model, Model, prop, modelAction } from "mobx-keystone";

@model("Item")
export class Item extends Model({
  task: prop<string>(),
  isDone: prop(false),
  id: prop<string>(),
  created: prop<string>(new Date().toDateString()),
}) {
  @modelAction
  markDone() {
    this.isDone = !this.isDone;
    console.log("check if being called");
  }
  @modelAction
  edit(editedTask: string) {
    this.task = editedTask;
  }
}
