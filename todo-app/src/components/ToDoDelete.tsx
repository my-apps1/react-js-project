import React, { useState, useContext, FormEvent, ChangeEvent } from "react";
import Store from "../models/List";
import { Item } from "../models/Item";
import { observer } from "mobx-react-lite";

interface TodoDeleteProp {
  item: Item;
  setIsDelete: Function;
}

const TodoDelete: React.FC<TodoDeleteProp> = ({ item, setIsDelete }) => {
  const store = useContext(Store);

  return (
    <div className="">
      Are you sure you want to delete to do item:{" "}
      <em>
        <b>{item.task}</b>
      </em>{" "}
      ?{"     "}
      <button
        className="deleteBtn"
        onClick={() => {
          store.deleteItem(item.id);
          setIsDelete(false);
        }}
      >
        Yes
      </button>
      <button className="noBtn" onClick={() => setIsDelete(false)}>
        No
      </button>
    </div>
  );
};
export default observer(TodoDelete);
