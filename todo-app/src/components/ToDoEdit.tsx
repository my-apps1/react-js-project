import React, { useState, useContext, FormEvent, ChangeEvent } from "react";
import Store from "../models/List";
import { Item } from "../models/Item";
import { observer } from "mobx-react-lite";

interface TodoEditProp {
  item: Item;
  setIsEdit: Function;
}

const TodoEdit: React.FC<TodoEditProp> = ({ item, setIsEdit }) => {
  const store = useContext(Store);
  const [editList, setEditList] = useState(item.task);

  const handleTodoEdit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    store.editItem(item.id, editList);
    setIsEdit(false);
    setEditList("");
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEditList(e.target.value);
  };

  return (
    <div className="">
      <form onSubmit={handleTodoEdit}>
        <input
          type="text"
          className="editTodo"
          value={editList}
          onChange={handleChange}
        />
        <button className="doneBtn" disabled={editList.length === 0}>
          Done
        </button>
        <button className="deleteBtn" onClick={() => setIsEdit(false)}>
          Cancel
        </button>
      </form>
    </div>
  );
};

export default observer(TodoEdit);
