import "mobx-react-lite/batchingForReactDom";
import React from "react";
import TodoList from "./TodoList";
import TodoAdd from "./TodoAdd";
import { observer } from "mobx-react-lite";

const TodoMain = () => {
  return (
    <div>
      <h1 className="appHeader"> To Do </h1>
      <TodoAdd />
      <TodoList />
    </div>
  );
};

export default observer(TodoMain);
